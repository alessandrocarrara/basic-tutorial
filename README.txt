SOMO FARMYARD 1.0.0

In order to successfully run this tutorial, you need

- JBoss EAP 6.2 or more with full profile (for JMS)
- MySQL DB (util/db.sql contains table creation script) 'farmyard'
- MySQL Datasource defined in JBoss similar to the following:
                <xa-datasource jndi-name="java:jboss/jdbc/farmyarddatasource" pool-name="farmyarddatasource" enabled="true" use-ccm="false">
                    <xa-datasource-property name="ServerName">
                        localhost
                    </xa-datasource-property>
                    <xa-datasource-property name="DatabaseName">
                        farmyard
                    </xa-datasource-property>
                    <xa-datasource-property name="PortNumber">
                        3306
                    </xa-datasource-property>
                    <xa-datasource-class>com.mysql.jdbc.jdbc2.optional.MysqlXADataSource</xa-datasource-class>
                    <driver>mysql</driver>
                    <xa-pool>
                        <is-same-rm-override>false</is-same-rm-override>
                        <interleaving>false</interleaving>
                        <pad-xid>false</pad-xid>
                        <wrap-xa-resource>false</wrap-xa-resource>
                    </xa-pool>
                    <security>
                        <user-name>root</user-name>
                        <password>usualpass</password>
                    </security>
                    <validation>
                        <check-valid-connection-sql>SELECT 1</check-valid-connection-sql>
                        <validate-on-match>false</validate-on-match>
                        <background-validation>false</background-validation>
                    </validation>
                    <statement>
                        <share-prepared-statements>false</share-prepared-statements>
                    </statement>
                </xa-datasource>

- JMS environment in JBoss; connection factories and queues as follows:
                    <connection-factory name="ButcherConnectionFactory">
                        <connectors>
                            <connector-ref connector-name="in-vm"/>
                        </connectors>
                        <entries>
                            <entry name="java:/jms/ButcherConnectionFactory"/>
                        </entries>
                        <compress-large-messages>false</compress-large-messages>
                        <failover-on-initial-connection>false</failover-on-initial-connection>
                        <use-global-pools>true</use-global-pools>
                    </connection-factory>

                AND

                <jms-destinations>
                    <jms-queue name="ButcherQueue">
                        <entry name="java:/jms/ButcherQueue"/>
                        <durable>true</durable>
                    </jms-queue>
                </jms-destinations>

- JMS user butcher/$omaro99, with consume permission

Enjoy!