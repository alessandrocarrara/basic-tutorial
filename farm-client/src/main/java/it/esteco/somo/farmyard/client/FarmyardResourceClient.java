/*
 * Copyright (C) ESTECO srl
 */
package it.esteco.somo.farmyard.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.impl.MultiPartConfigProvider;
import com.sun.jersey.multipart.impl.MultiPartWriter;
import it.esteco.somo.farmyard.animals.Animal;
import it.esteco.somo.farmyard.animals.Bunny;
import it.esteco.somo.farmyard.animals.Cow;
import it.esteco.somo.farmyard.animals.Pig;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author sergio
 */
public class FarmyardResourceClient {

    protected final Client client;
    /**
     * The base URL of the remote SOMO application
     */
    protected String serverUrl = null;

    public FarmyardResourceClient(String serverUrl, boolean logHttpRequests) {

        ClientConfig config = new DefaultClientConfig();
        config.getClasses().add(MultiPartConfigProvider.class);
        config.getClasses().add(MultiPartWriter.class);
        config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);

        config.getProperties().put("com.sun.jersey.config.property.packages", getPackages());

        client = Client.create(config);

        client.removeAllFilters();
        // Uncomment this to log the HTTP requests and responses that go through the client
        if (logHttpRequests) {
            client.addFilter(new LoggingFilter(System.out));
        }
        client.setFollowRedirects(Boolean.TRUE);
        this.serverUrl = removeSlash(serverUrl);
    }

    private String removeSlash(String serverUrl) {
        try {
            serverUrl = new URI(serverUrl).normalize().toString();
            return serverUrl.endsWith("/") ? serverUrl.substring(0, serverUrl.length() - 1) : serverUrl;
        } catch (URISyntaxException ex) {
            Logger.getLogger(FarmyardResourceClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return serverUrl;
    }

    private String getPackages() {
        StringBuilder sb = new StringBuilder();
        sb.append("com.fasterxml.jackson.jaxrs.json,");
        sb.append("it.esteco.somo.farmyard.animals");
        return sb.toString();
    }

    public Pig createPig(String name) {
        return createAnimal(name, Pig.class);
    }

    public Cow createCow(String name) {
        return createAnimal(name, Cow.class);
    }

    public Bunny createBunny(String name) {
        return createAnimal(name, Bunny.class);
    }

    private <T extends Animal> T createAnimal(String name, Class<T> animalClass) {
        URI uri = UriBuilder.fromUri(serverUrl + "/rest/farmyard/" + animalClass.getSimpleName().toLowerCase()).build();
        WebResource resource = client.resource(uri);

        FormDataMultiPart multipart = new FormDataMultiPart();

        FormDataBodyPart nameBodypart = new FormDataBodyPart("name", name, MediaType.TEXT_PLAIN_TYPE);
        multipart.bodyPart(nameBodypart);
        ClientResponse resp = resource.type(MediaType.MULTIPART_FORM_DATA_TYPE).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class, multipart);

        return resp.getEntity(animalClass);
    }

    public String butcherAnimal() {
        URI uri = UriBuilder.fromUri(serverUrl + "/rest/farmyard/butcher").build();
        WebResource resource = client.resource(uri);
        ClientResponse resp = resource.accept(MediaType.TEXT_PLAIN).post(ClientResponse.class);
        return resp.getEntity(String.class);
    }

    public void sow() {
        URI uri = UriBuilder.fromUri(serverUrl + "/rest/farmyard/sow").build();
        WebResource resource = client.resource(uri);
        resource.post(ClientResponse.class);
    }
}
