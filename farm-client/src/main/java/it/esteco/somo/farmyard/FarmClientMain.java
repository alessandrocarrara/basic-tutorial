package it.esteco.somo.farmyard;

import it.esteco.somo.farmyard.animals.Bunny;
import it.esteco.somo.farmyard.animals.Cow;
import it.esteco.somo.farmyard.animals.Pig;
import it.esteco.somo.farmyard.client.FarmyardResourceClient;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 15, 2015 3:38 PM.
 */
public class FarmClientMain {

    public static void main(String[] args) {
        FarmyardResourceClient farmyardResourceClient = new FarmyardResourceClient("http://localhost:8080/farmyard-web", false);

        Pig pig = farmyardResourceClient.createPig("Lello Porcello");
        Cow cow = farmyardResourceClient.createCow("Elisabetta Vacchetta");
        Bunny bunny = farmyardResourceClient.createBunny("Stoviglio Coniglio");
        System.out.println(pig + " is born!");
        System.out.println(cow + " is born!");
        System.out.println(bunny + " is born!");

        for (int i = 0; i < 1; i++) {
            System.out.println(farmyardResourceClient.butcherAnimal());
        }
    }

}
