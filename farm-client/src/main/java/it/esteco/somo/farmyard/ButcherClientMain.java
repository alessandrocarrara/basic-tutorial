package it.esteco.somo.farmyard;

import it.esteco.somo.farmyard.animals.Animal;
import it.esteco.somo.farmyard.jms.ButcherClientFactory;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 16, 2015 11:04 AM.
 */
public class ButcherClientMain {

    public static void main(String[] args) throws JMSException {

        ButcherClientFactory butcherClientFactory = new ButcherClientFactory();
        Connection connection = butcherClientFactory.getConnection();

        System.out.println("Butcher ready to work!");

        try {
            connection.start();
            MessageConsumer consumer = createConsumer(connection);
            while (true) {
                Message message = consumer.receive(5000);
                onMessage(message);
            }
        } finally {
            System.out.println("Butcher going to sleep...");
            connection.stop();
            connection.close();
        }

    }

    private static void onMessage(Message message) throws JMSException {
        ObjectMessage objectMessage = (ObjectMessage) message;
        if (objectMessage != null) {
            Animal animal = (Animal) objectMessage.getObject();
            System.out.println(animal.talk() + ", I hope you choke on my meat, you fucking carnivore");
        }
    }

    private static MessageConsumer createConsumer(Connection connection) throws JMSException {
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Queue queue = session.createQueue("ButcherQueue");
        return session.createConsumer(queue);
    }
}
