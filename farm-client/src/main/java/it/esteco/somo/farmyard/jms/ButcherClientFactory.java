package it.esteco.somo.farmyard.jms;

import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.api.jms.HornetQJMSClient;
import org.hornetq.api.jms.JMSFactoryType;
import org.hornetq.core.remoting.impl.netty.NettyConnectorFactory;
import org.hornetq.core.remoting.impl.netty.TransportConstants;
import org.hornetq.jms.client.HornetQConnectionFactory;

import javax.jms.Connection;
import javax.jms.JMSException;
import java.util.HashMap;
import java.util.Map;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 16, 2015 11:06 AM.
 */
public class ButcherClientFactory {

    public Connection getConnection() throws JMSException {
        String jmsHostname = "localhost";
        String jmsPort = "5445";
        String jmsUsername = "butcher";
        String jmsPassword = "$omaro99";

        Map<String, Object> connectionParams = new HashMap<String, Object>();
        connectionParams.put(TransportConstants.HOST_PROP_NAME, jmsHostname);
        connectionParams.put(TransportConstants.PORT_PROP_NAME, Integer.valueOf(jmsPort));

        TransportConfiguration tc = new TransportConfiguration(NettyConnectorFactory.class.getName(), connectionParams);
        HornetQConnectionFactory connectionFactory = HornetQJMSClient.createConnectionFactoryWithoutHA(JMSFactoryType.CF, tc);
        connectionFactory.setReconnectAttempts(-1);
        connectionFactory.setConsumerWindowSize(0);
        connectionFactory.setConsumerMaxRate(1);

        return connectionFactory.createConnection(jmsUsername, jmsPassword);
    }

}
