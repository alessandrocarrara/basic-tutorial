package it.esteco.somo.farmyard;

import it.esteco.somo.farmyard.client.FarmyardResourceClient;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 15, 2015 3:38 PM.
 */
public class HarvesterClientMain {

    public static void main(String[] args) {
        FarmyardResourceClient farmyardResourceClient = new FarmyardResourceClient("http://localhost:8080/farmyard-web", false);

        farmyardResourceClient.sow();
        System.out.println("Harvest!");
    }

}
