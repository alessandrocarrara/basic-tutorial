package it.esteco.somo.farmyard.ejb.jms;

import it.esteco.somo.farmyard.animals.Animal;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;
import javax.jms.Session;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 10, 2015 12:30 PM.
 */
@Stateless
public class AnimalButcherBroker {

    @Resource(name = "jms/ButcherConnectionFactory")
    private QueueConnectionFactory butcherConnectionFactory;
    @Resource(name = "jms/ButcherQueue")
    private Queue butcherQueue;

    public void butcherAnimal(Animal animal) {
        Connection connection = null;
        try {
            connection = butcherConnectionFactory.createConnection();
            Session session = connection.createSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
            MessageProducer butcherCommandProducer = session.createProducer(butcherQueue);

            ObjectMessage message = session.createObjectMessage();
            message.setObject(animal);

            butcherCommandProducer.send(message);

            Logger.getLogger(AnimalButcherBroker.class.getName()).log(Level.INFO, "Animal " + animal.getName() + " sent to butcher!");
        } catch (JMSException ex) {
            Logger.getLogger(AnimalButcherBroker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            safeCloseConnection(connection);
        }
    }

    private void safeCloseConnection(Connection connection) {
        try {
            if(connection != null) {
                connection.close();
            }
        } catch (JMSException e) {
            Logger.getLogger(AnimalButcherBroker.class.getName()).log(Level.SEVERE, null, e);
        }
    }

}
