package it.esteco.somo.farmyard.ejb;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 02, 2015 11:33 AM.
 */
@ApplicationPath("rest")
public class RestConfiguration extends Application {
}
