package it.esteco.somo.farmyard.ejb.food;

import it.esteco.somo.farmyard.animals.Animal;
import it.esteco.somo.farmyard.ejb.jpa.controller.AnimalController;
import it.esteco.somo.farmyard.ejb.jpa.controller.FoodController;
import it.esteco.somo.farmyard.ejb.jpa.entity.AnimalEntity;
import it.esteco.somo.farmyard.ejb.jpa.entity.FoodEntity;
import it.esteco.somo.farmyard.exception.AnimalException;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Aug 13, 2015 6:36 PM.
 */
@Stateless
public class Trough {

    private static final int SECOND = 1000;
    private static final int MINUTE = 60 * SECOND;
    @EJB
    private FoodController foodController;
    @EJB
    private AnimalController animalController;
    @Resource
    private TimerService     dinnerTime;

    @Inject
    private Food food;

    @PostConstruct
    public void postConstruct() {
        createTimer();
    }

    public Food addFood(int quantity) {
        FoodEntity entity = getFoodEntity();
        entity.addQuantity(quantity);
        foodController.edit(entity);
        food.add(quantity);
        Logger.getLogger(getClass().getCanonicalName()).log(Level.INFO, String.format("Trough has %d Kgs of hay", food.getQuantity()));
        return food;
    }

    private FoodEntity getFoodEntity() {
        FoodEntity entity;
        if (foodController.countEntities() == 0) {
            entity = new FoodEntity();
            entity.setType(food.getType());
            entity.setQuantity(0);
            foodController.create(entity);
        } else {
            entity = foodController.findEntities().get(0);
            food.setQuantity(entity.getQuantity());
        }
        return entity;
    }

    @Timeout
    public void dinnerTime(Timer timer) {
        try {
            feedAnimals();
        } catch (AnimalException e) {
            Logger.getLogger(getClass().getCanonicalName()).log(Level.SEVERE, null, e);
        } finally {
            rescheduleTimers();
        }
    }

    private void feedAnimals() throws AnimalException {
        List<AnimalEntity> animalEntities = animalController.findEntities();
        if(animalEntities.isEmpty()) {
            Logger.getLogger(getClass().getCanonicalName()).log(Level.INFO, "No animals, tristessa");
        }
        for (AnimalEntity animalEntity : animalEntities) {
            Animal animal = animalEntity.toAnimal();
            if(!isEmpty()) {
                food.remove(animal.eat());
                Logger.getLogger(getClass().getCanonicalName()).log(Level.INFO, String.format("Animal %s burps", animal.getName()));
                Logger.getLogger(getClass().getCanonicalName()).log(Level.INFO, String.format("Trough has %d Kg of hay", food.getQuantity()));
            } else {
                animalController.remove(animalEntity);
                Logger.getLogger(getClass().getCanonicalName()).log(Level.INFO, String.format("Animal %s has died of starvation...", animal.getName()));
            }
        }
    }

    private boolean isEmpty() {
        return food.getQuantity() > 0;
    }

    private void rescheduleTimers() {
        cancelTimers();
        createTimer();
    }

    private void createTimer() {
        Date nextDate = new Date(System.currentTimeMillis() + SECOND);
        dinnerTime.createSingleActionTimer(nextDate, new TimerConfig(getClass().getName(), false));
    }

    private void cancelTimers() {
        Collection<Timer> timers = dinnerTime.getTimers();
        for (Timer timer : timers) {
            if (getClass().getName().equals(timer.getInfo())) {
                timer.cancel();
            }
        }
    }

}
