package it.esteco.somo.farmyard.ejb.jpa.controller;

import it.esteco.somo.farmyard.ejb.jpa.entity.FoodEntity;
import it.esteco.somo.farmyard.web.servlets.EntityManagerLoaderListener;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 10, 2015 3:28 PM.
 */
@Stateless
public class FoodController {


    private EntityManager em = EntityManagerLoaderListener.createEntityManager();

    public void create(FoodEntity entity) {
        em.persist(entity);
    }

    public void remove(FoodEntity entity) {
        em.remove(entity);
    }

    public FoodEntity edit(FoodEntity entity) {
        return em.merge(entity);
    }

    public FoodEntity findEntitySafe(Long id) {
        Query q = em.createQuery("select e from FoodEntity e where e.id = :id");
        q.setParameter("id", id);
        return (FoodEntity) q.getSingleResult();
    }

    public List<FoodEntity> findEntities() {
        return findEntities(true, -1, -1);
    }

    public List<FoodEntity> findEntities(int maxResults, int firstResult) {
        return findEntities(false, maxResults, firstResult);
    }

    private List<FoodEntity> findEntities(boolean all, int maxResults, int firstResult) {
        Query q = em.createQuery("SELECT e FROM FoodEntity e");
        if (!all) {
            q.setMaxResults(maxResults); //limit
            q.setFirstResult(firstResult); //offset
        }
        return q.getResultList();
    }

    public Long countEntities() {
        Query q = em.createQuery("SELECT COUNT(e) FROM FoodEntity e");
        return (Long) q.getSingleResult();
    }
}
