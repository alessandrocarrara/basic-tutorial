package it.esteco.somo.farmyard.ejb.food.exception;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 16, 2015 12:16 PM.
 */
public class UnrecognizedFoodException extends IllegalArgumentException {

    public UnrecognizedFoodException(int type) {
        super("Unrecognized food type: " + type);
    }
}
