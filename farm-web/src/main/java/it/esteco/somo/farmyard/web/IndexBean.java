package it.esteco.somo.farmyard.web;

import it.esteco.somo.farmyard.animals.Animal;
import it.esteco.somo.farmyard.ejb.FarmYard;
import it.esteco.somo.farmyard.exception.AnimalException;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 16, 2015 12:25 PM.
 */
@ManagedBean(name = "index")
@SessionScoped
public class IndexBean {

    @EJB
    private FarmYard farmYard;
    private Collection<Animal> animals;

    @PostConstruct
    public void post() {
        try {
            animals = farmYard.getAnimals();
        } catch (AnimalException ex) {
            animals = new HashSet<Animal>();
            Logger.getLogger(IndexBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getFarmyardName() {
        return "SOMO Farmyard";
    }

    public Collection<Animal> getAnimals() {
        return animals;
    }
}
