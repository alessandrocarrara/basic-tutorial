package it.esteco.somo.farmyard.ejb.food;

import javax.enterprise.inject.Alternative;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Aug 13, 2015 6:24 PM.
 */
@Alternative
public class Hay extends QuantifiableFood {

    public Hay() {
        super(10);
    }

    public FoodType getType() {
        return FoodType.HAY;
    }
}
