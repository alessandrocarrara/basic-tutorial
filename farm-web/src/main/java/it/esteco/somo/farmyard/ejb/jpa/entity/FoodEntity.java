package it.esteco.somo.farmyard.ejb.jpa.entity;

import it.esteco.somo.farmyard.ejb.food.FoodType;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 10, 2015 3:28 PM.
 */
@Entity
@Table(name = "food")
public class FoodEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long    id;
    @Column(name = "quantity")
    @Basic(optional = false)
    private int     quantity;
    @Column(name = "type")
    @Basic(optional = false)
    private Integer type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public FoodType getType() {
        return FoodType.fromOrdinal(type);
    }

    public void setType(FoodType type) {
        this.type = type.ordinal();
    }

    public void addQuantity(int quantity) {
        this.quantity += quantity;
    }
}
