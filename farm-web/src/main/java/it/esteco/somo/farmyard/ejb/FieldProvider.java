package it.esteco.somo.farmyard.ejb;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Sep 30, 2015 10:32 AM.
 */
@Singleton
@Startup
public class FieldProvider {

    private static final long GROWING_TIME = 10000L;

    @Resource
    private TimerService timerService;

    public void createField() {
        TimerConfig timerConfig = new TimerConfig();
        timerConfig.setInfo("field growing");
        timerService.createSingleActionTimer(GROWING_TIME, timerConfig);
    }

    @Timeout
    public void harvest() {
        Logger.getLogger(getClass().getCanonicalName()).log(Level.INFO, "harvesting!");
    }

}
