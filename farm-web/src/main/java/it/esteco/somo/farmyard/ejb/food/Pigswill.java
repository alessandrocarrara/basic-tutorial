package it.esteco.somo.farmyard.ejb.food;

import javax.enterprise.inject.Default;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Aug 13, 2015 6:24 PM.
 */
@Default
public class Pigswill extends QuantifiableFood {

    public Pigswill() {
        super(500);
    }

    public FoodType getType() {
        return FoodType.PIGSWILL;
    }
}
