package it.esteco.somo.farmyard.ejb;

import com.sun.jersey.multipart.FormDataParam;
import it.esteco.somo.farmyard.animals.Bunny;
import it.esteco.somo.farmyard.animals.Cow;
import it.esteco.somo.farmyard.animals.Pig;
import it.esteco.somo.farmyard.exception.AnimalException;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 02, 2015 10:05 AM.
 */
@Path("farmyard")
@Stateless
public class FarmYardResource {

    private FarmYard farmYard;

    @PostConstruct
    private void post() {
        try {
            farmYard = (FarmYard) new InitialContext().lookup("java:module/" + FarmYard.class.getSimpleName());
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    @POST
    @Path("pig")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response createPig(@Context UriInfo uriInfo, @FormDataParam("name") String name) {
        try {
            Pig pig = farmYard.createPig(name);
            URI newResourceUri = uriInfo.getAbsolutePathBuilder().path(pig.getName()).build();
            return Response.created(newResourceUri).entity(pig).build();
        } catch (AnimalException e) {
            return Response.serverError().entity(e.getMessage()).type(MediaType.TEXT_PLAIN_TYPE).build();
        }
    }

    @POST
    @Path("cow")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response createCow(@Context UriInfo uriInfo, @FormDataParam("name") String name) {
        try {
            Cow cow = farmYard.createCow(name);
            URI newResourceUri = uriInfo.getAbsolutePathBuilder().path(cow.getName()).build();
            return Response.created(newResourceUri).entity(cow).build();
        } catch (AnimalException e) {
            return Response.serverError().entity(e.getMessage()).type(MediaType.TEXT_PLAIN_TYPE).build();
        }
    }

    @POST
    @Path("bunny")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response createBunny(@Context UriInfo uriInfo, @FormDataParam("name") String name) {
        try {
            Bunny bunny = farmYard.createBunny(name);
            URI newResourceUri = uriInfo.getAbsolutePathBuilder().path(bunny.getName()).build();
            return Response.created(newResourceUri).entity(bunny).build();
        } catch (AnimalException e) {
            return Response.serverError().entity(e.getMessage()).type(MediaType.TEXT_PLAIN_TYPE).build();
        }
    }

    @POST
    @Path("butcher")
    @Produces(MediaType.TEXT_PLAIN)
    public Response butcherAnimal() {
        try {
            boolean butchered = farmYard.butcherAnimal();
            if(butchered) {
                return Response.ok().entity("Butchered some meat").build();
            } else {
                return Response.ok().entity("Nothing to butcher").build();
            }
        } catch (AnimalException e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @POST
    @Path("sow")
    @Produces(MediaType.TEXT_PLAIN)
    public Response sow() {
        farmYard.sow();
        return Response.noContent().build();
    }

}
