package it.esteco.somo.farmyard.ejb.food;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Aug 14, 2015 7:18 AM.
 */
public abstract class QuantifiableFood implements Food {

    private final int calories;
    private int quantity = 0;

    QuantifiableFood(int calories) {
        this.calories = calories;
    }

    public void remove(int eat) {
        quantity = quantity > eat ? quantity - eat : 0;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getCalories() {
        return calories;
    }

    public void add(int quantity) {
        this.quantity += quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return quantity + " kgs of " + getClass().getSimpleName();
    }
}
