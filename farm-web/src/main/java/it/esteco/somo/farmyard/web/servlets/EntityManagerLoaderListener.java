package it.esteco.somo.farmyard.web.servlets;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Initialize EntityManager Factory
 * For Heroku, try to find the environment property DATABASE_URL, and transform
 * it into a valid jdbc URL to initialize properly the DB.
 *
 * @author mlecoutre
 */
@WebListener
public class EntityManagerLoaderListener implements ServletContextListener {
    private static final Logger logger = Logger.getLogger(EntityManagerLoaderListener.class.getCanonicalName());

    private static EntityManagerFactory emf;

    public EntityManagerLoaderListener() {
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        logger.log(Level.INFO, "WebListener start entity manager");

        String databaseUrl = System.getenv("DATABASE_URL");
        Map<String, String> properties = new HashMap<String, String>();
        if (databaseUrl != null) {

            StringTokenizer st = new StringTokenizer(databaseUrl, ":@/");
            String dbVendor = st.nextToken(); //if DATABASE_URL is set
            String userName = st.nextToken();
            String password = st.nextToken();
            String host = st.nextToken();
            String port = st.nextToken();
            String databaseName = st.nextToken();
            String jdbcUrl = String.format("jdbc:postgresql://%s:%s/%s?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory", host, port, databaseName);
            properties.put("javax.persistence.jdbc.url", jdbcUrl);
            properties.put("javax.persistence.jdbc.user", userName);
            properties.put("javax.persistence.jdbc.password", password);
            properties.put("javax.persistence.jdbc.driver", "org.postgresql.Driver");
            properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        }
        emf = Persistence.createEntityManagerFactory("farmyard-persistence-unit", properties);
    }

    /**
     * Close the entity manager
     *
     * @param event ServletContextEvent not used
     */
    @Override
    public void contextDestroyed(ServletContextEvent event) {
        emf.close();
    }

    /**
     * Create the EntityManager
     *
     * @return EntityManager
     */
    public static EntityManager createEntityManager() {
        if (emf == null) {
            throw new IllegalStateException("Context is not initialized yet.");
        }

        return emf.createEntityManager();
    }
}
