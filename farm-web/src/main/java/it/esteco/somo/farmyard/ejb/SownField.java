package it.esteco.somo.farmyard.ejb;

import it.esteco.somo.farmyard.ejb.food.Trough;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Aug 17, 2015 7:56 AM.
 */
@Stateless
public class SownField {

    @Asynchronous
    public void grow(Trough trough) {
        for (int i = 0; i < 10; i++) {
            sleep();
            Logger.getLogger(getClass().getCanonicalName()).log(Level.INFO, String.format("Harvest grown to %d percent", i * 10));
        }
        trough.addFood(100);
    }

    private void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Logger.getLogger(getClass().getCanonicalName()).log(Level.SEVERE, null, e);
        }
    }

}
