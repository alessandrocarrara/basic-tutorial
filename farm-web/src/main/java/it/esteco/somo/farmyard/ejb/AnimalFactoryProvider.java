package it.esteco.somo.farmyard.ejb;

import it.esteco.somo.farmyard.animals.AnimalFactory;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 02, 2015 9:52 AM.
 */
@Startup
@Singleton
public class AnimalFactoryProvider {

    private AnimalFactory animalFactory;

    @PostConstruct
    private void post() {
        animalFactory = new AnimalFactory();
        Logger.getLogger(AnimalFactoryProvider.class.getCanonicalName()).log(Level.INFO, "Creating animal factory...");
    }

    public AnimalFactory getAnimalFactory() {
        return animalFactory;
    }

}
