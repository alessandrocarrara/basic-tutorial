package it.esteco.somo.farmyard.ejb;

import it.esteco.somo.farmyard.animals.Animal;
import it.esteco.somo.farmyard.animals.AnimalBreed;
import it.esteco.somo.farmyard.animals.Bunny;
import it.esteco.somo.farmyard.animals.Cow;
import it.esteco.somo.farmyard.animals.Pig;
import it.esteco.somo.farmyard.ejb.jms.AnimalButcherBroker;
import it.esteco.somo.farmyard.ejb.jpa.controller.AnimalController;
import it.esteco.somo.farmyard.ejb.jpa.entity.AnimalEntity;
import it.esteco.somo.farmyard.exception.AnimalException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Collection;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 02, 2015 9:57 AM.
 */
@Stateless
public class FarmYard {

    @EJB
    private AnimalFactoryProvider animalFactoryProvider;
    @EJB
    private AnimalController animalController;
    @EJB
    private AnimalButcherBroker animalButcherBroker;
    @EJB
    private FieldProvider fieldProvider;

    public Pig createPig(String name) throws AnimalException {
        AnimalEntity animalEntity = addDbEntity(name, AnimalBreed.PIG);
        return (Pig) animalEntity.toAnimal();
    }

    public Cow createCow(String name) throws AnimalException {
        AnimalEntity animalEntity = addDbEntity(name, AnimalBreed.COW);
        return (Cow) animalEntity.toAnimal();
    }

    public Bunny createBunny(String name) throws AnimalException {
        AnimalEntity animalEntity = addDbEntity(name, AnimalBreed.BUNNY);
        return (Bunny) animalEntity.toAnimal();
    }

    private AnimalEntity addDbEntity(String name, AnimalBreed breed) {
        AnimalEntity animalEntity = new AnimalEntity();
        animalEntity.setName(name);
        animalEntity.setType(breed);
        animalController.create(animalEntity);
        return animalEntity;
    }

    public boolean butcherAnimal() throws AnimalException {
        Long entities = animalController.countEntities();
        if (entities <= 0) {
            return false;
        }
        AnimalEntity animalEntity = animalController.findEntities().get(0);
        Animal animal = animalEntity.toAnimal();
        animalButcherBroker.butcherAnimal(animal);
        animalController.remove(animalEntity);
        return true;
    }

    public Collection<Animal> getAnimals() throws AnimalException {
        return animalController.findEntitiesAsAnimals();
    }

    public void sow() {
        fieldProvider.createField();
    }
}
