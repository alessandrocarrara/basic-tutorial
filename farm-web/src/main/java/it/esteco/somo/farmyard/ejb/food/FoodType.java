package it.esteco.somo.farmyard.ejb.food;

import it.esteco.somo.farmyard.ejb.food.exception.UnrecognizedFoodException;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Aug 14, 2015 7:24 AM.
 */
public enum FoodType {

    HAY,
    PIGSWILL;

    public static FoodType fromOrdinal(Integer type) {
        for (FoodType foodType : values()) {
            if (foodType.ordinal() == type) {
                return foodType;
            }
        }
        throw new UnrecognizedFoodException(type);
    }

}
