package it.esteco.somo.farmyard.ejb.async;

import it.esteco.somo.farmyard.animals.AnimalBreed;
import it.esteco.somo.farmyard.ejb.jpa.controller.AnimalController;
import it.esteco.somo.farmyard.ejb.jpa.entity.AnimalEntity;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Aug 11, 2015 4:23 PM.
 */
@Stateless
public class BunnyCreator {

    @EJB
    private AnimalController animalController;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public AnimalEntity create(String name) {
        AnimalEntity animalEntity = new AnimalEntity();
        animalEntity.setName(name);
        animalEntity.setType(AnimalBreed.BUNNY);
        animalController.create(animalEntity);

        return animalEntity;
    }

}
