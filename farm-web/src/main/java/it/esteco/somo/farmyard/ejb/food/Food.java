package it.esteco.somo.farmyard.ejb.food;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Aug 13, 2015 6:23 PM.
 */
public interface Food {

    int getCalories();

    void add(int quantity);

    FoodType getType();

    void setQuantity(int quantity);

    void remove(int eat);

    int getQuantity();
}
