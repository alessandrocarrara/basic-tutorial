package it.esteco.somo.farmyard.ejb.async;

import it.esteco.somo.farmyard.ejb.jpa.controller.AnimalController;
import it.esteco.somo.farmyard.ejb.jpa.entity.AnimalEntity;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Aug 11, 2015 4:23 PM.
 */
@Stateless
public class BunnyModifier {

    @EJB
    private AnimalController animalController;

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void changeName(AnimalEntity animalEntity) {
        animalEntity.setName("cambiato");
        animalController.edit(animalEntity);
    }

}
