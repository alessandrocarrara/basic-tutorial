package it.esteco.somo.farmyard.ejb.jpa.entity;

import it.esteco.somo.farmyard.animals.Animal;
import it.esteco.somo.farmyard.animals.AnimalBreed;
import it.esteco.somo.farmyard.animals.AnimalFactory;
import it.esteco.somo.farmyard.exception.AnimalException;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 10, 2015 3:28 PM.
 */
@Entity
@Table(name = "animals")
@NamedQueries({
        @NamedQuery(name = "AnimalEntity.findAnimalsByName", query = "SELECT a FROM AnimalEntity a WHERE a.name = :name ORDER BY a.id DESC")
})
public class AnimalEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    @Basic(optional = false)
    private String name;
    @Column(name = "type")
    @Basic(optional = false)
    private Integer type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AnimalBreed getType() {
        return AnimalBreed.fromOrdinal(type);
    }

    public void setType(AnimalBreed type) {
        this.type = type.ordinal();
    }

    public Animal toAnimal() throws AnimalException {
        AnimalFactory animalFactory = new AnimalFactory();
        return animalFactory.createAnimal(getType(), getName());
    }
}
