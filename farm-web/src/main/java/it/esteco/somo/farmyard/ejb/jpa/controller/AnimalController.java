package it.esteco.somo.farmyard.ejb.jpa.controller;

import it.esteco.somo.farmyard.animals.Animal;
import it.esteco.somo.farmyard.ejb.jpa.entity.AnimalEntity;
import it.esteco.somo.farmyard.exception.AnimalException;
import it.esteco.somo.farmyard.web.servlets.EntityManagerLoaderListener;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 10, 2015 3:28 PM.
 */
@Stateless
public class AnimalController {

    private EntityManager em = EntityManagerLoaderListener.createEntityManager();

    public void create(AnimalEntity entity) {
        em.persist(entity);
    }

    public void remove(AnimalEntity entity) {
        em.remove(entity);
    }

    public AnimalEntity edit(AnimalEntity entity) {
        return em.merge(entity);
    }

    public AnimalEntity findEntitySafe(Long id) {
        Query q = em.createQuery("select e from AnimalEntity e where e.id = :id");
        q.setParameter("id", id);
        return (AnimalEntity) q.getSingleResult();
    }

    public List<AnimalEntity> findEntities() {
        return findEntities(true, -1, -1);
    }

    public List<AnimalEntity> findEntities(int maxResults, int firstResult) {
        return findEntities(false, maxResults, firstResult);
    }

    private List<AnimalEntity> findEntities(boolean all, int maxResults, int firstResult) {
        Query q = em.createQuery("SELECT e FROM AnimalEntity e");
        if (!all) {
            q.setMaxResults(maxResults); //limit
            q.setFirstResult(firstResult); //offset
        }
        return q.getResultList();
    }

    public Long countEntities() {
        Query q = em.createQuery("SELECT COUNT(e) FROM AnimalEntity e");
        return (Long) q.getSingleResult();
    }

    public List<Animal> findEntitiesAsAnimals() throws AnimalException {
        List<Animal> animals = new ArrayList<Animal>();
        Collection<AnimalEntity> animalEntities = findEntities();
        for (AnimalEntity animalEntity : animalEntities) {
            animals.add(animalEntity.toAnimal());
        }
        return animals;
    }
}
