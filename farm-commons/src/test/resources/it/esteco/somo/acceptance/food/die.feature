Feature: Dying because of food shortage

  Scenario Outline: animal not eating food and dying
    Given one animal of breed <breed>
    And a food with quantity <quantity>
    When the animal eats for <times> times
    Then the animal has <died>

    Examples:
    | breed | quantity | times |  died |
    | bunny |    0     |   1   | false |
    | bunny |    0     |   2   | true  |
    | cow   |    0     |   5   | false |
    | cow   |    0     |   10  | true  |
    | pig   |    0     |   5   | false |
    | pig   |    0     |   10  | true  |
    | pig   |    0     |   11  | true  |