Feature: Eating food

  Scenario Outline: animal eating food
    Given one animal of breed <breed>
    And a food with quantity <quantity>
    When the animal eats
    Then the food quantity is <remaining>

    Examples:
    | breed | quantity | remaining |
    | bunny |    50    |    49     |
    | bunny |    0     |    0      |
    | cow   |    50    |    30     |
    | cow   |    1     |    0      |
    | pig   |    50    |    40     |
    | pig   |    1     |    0      |