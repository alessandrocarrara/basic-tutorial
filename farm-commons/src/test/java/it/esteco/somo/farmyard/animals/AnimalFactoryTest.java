package it.esteco.somo.farmyard.animals;

import it.esteco.somo.farmyard.exception.AnimalException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 01, 2015 3:10 PM.
 */
public class AnimalFactoryTest {

    private AnimalFactory animalFactory;

    @Before
    public void setUp() throws Exception {
        animalFactory = new AnimalFactory();
    }

    @Test
    public void shouldCreateACat() throws Exception {
        Cow expected = new Cow();
        expected.setName("cici");
        Animal actual = animalFactory.createCow("cici");
        assertEquals(expected, actual);
    }

    @Test
    public void shouldThrowAnExceptionIfCatHasNullName() throws Exception {
        try {
            animalFactory.createCow(null);
            fail();
        } catch(AnimalException ex) {
            String expected = "Do you want to create a stray?";
            String actual = ex.getMessage();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void shouldCreateADog() throws Exception {
        Pig expected = new Pig();
        expected.setName("cici");
        expected.setTrained(true);
        Animal actual = animalFactory.createPig("cici", true);
        assertEquals(expected, actual);
    }

    @Test
    public void shouldThrowAnExceptionIfDogHasNullName() throws Exception {
        try {
            animalFactory.createPig(null, true);
            fail();
        } catch(AnimalException ex) {
            String expected = "Do you want to create a stray?";
            String actual = ex.getMessage();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void shouldCreateABunnyThatIsAnAnimal() throws Exception {
        Bunny expected = new Bunny();
        expected.setName("ciccio");
        Animal actual = animalFactory.createBunny("ciccio");
        assertEquals(expected, actual);
    }

    @Test
    public void shouldThrowExceptionWhenNameIsNull() {
        try {
            animalFactory.createBunny(null);
            fail();
        } catch (AnimalException e) {
            String expected = "Do you want to create a stray?";
            String actual = e.getMessage();
            assertEquals(expected, actual);
        }
    }

    @Test
    public void shouldReturnABunny() throws Exception {
        String name = "cicci";
        AnimalBreed animalBreed = AnimalBreed.BUNNY;

        Animal expected = new Bunny();
        expected.setName("cicci");

        Animal actual = animalFactory.createAnimal(animalBreed, name);
        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnACow() throws Exception {
        String name = "cicci";
        AnimalBreed animalBreed = AnimalBreed.COW;

        Animal expected = new Cow();
        expected.setName("cicci");

        Animal actual = animalFactory.createAnimal(animalBreed, name);
        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnAPig() throws Exception {
        String name = "cicci";
        AnimalBreed animalBreed = AnimalBreed.PIG;

        Pig expected = new Pig();
        expected.setName("cicci");
        expected.setTrained(true);

        Animal actual = animalFactory.createAnimal(animalBreed, name);
        assertEquals(expected, actual);
    }
}