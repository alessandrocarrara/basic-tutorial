package it.esteco.somo.farmyard.animals;

import it.esteco.somo.farmyard.exception.UnrecognizedBreedException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 16, 2015 12:13 PM.
 */
public class AnimalBreedTest {

    @Test
    public void shouldReturnCorrectBreed() throws Exception {
        assertEquals(AnimalBreed.COW, AnimalBreed.fromOrdinal(0));
        assertEquals(AnimalBreed.PIG, AnimalBreed.fromOrdinal(1));
        assertEquals(AnimalBreed.BUNNY, AnimalBreed.fromOrdinal(2));
    }

    @Test
    public void shouldThrowUnrecognizedBreedException() throws Exception {
        try {
            AnimalBreed.fromOrdinal(9);
            fail();
        } catch(UnrecognizedBreedException e) {
            String expected = "Unrecognized breed type: 9";
            String actual = e.getMessage();
            assertEquals(expected, actual);
        }
    }


}