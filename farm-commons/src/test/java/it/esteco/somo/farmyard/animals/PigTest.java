package it.esteco.somo.farmyard.animals;

import it.esteco.somo.farmyard.Food;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 01, 2015 2:51 PM.
 */
public class PigTest {

    private Pig pig;

    @Before
    public void setUp() throws Exception {
        pig = new Pig();
    }

    @Test
    public void ciciShouldBarkWhenTalking() throws Exception {
        pig.setName("cici");
        String expected = "Grunt grunt, I am cici";
        String actual = pig.talk();
        assertEquals(expected, actual);
    }

    @Test
    public void noOneShouldBarkWhenTalking() throws Exception {
        String expected = "Grunt grunt, I am no one";
        String actual = pig.talk();
        assertEquals(expected, actual);
    }

    @Test
    public void shouldBeEqualToSelf() throws Exception {
        assertTrue(pig.equals(pig));
    }

    @Test
    public void shouldHaveSameHashcode() throws Exception {
        Pig latter = new Pig();
        assertEquals(latter.hashCode(), pig.hashCode());
    }

    @Test
    public void shouldNotBeEqualToDifferentObject() throws Exception {
        assertFalse(pig.equals(new Object()));
    }

    @Test
    public void shouldBeEqualWhenNoNameIsSetAndIsNotTrained() throws Exception {
        Pig latter = new Pig();
        assertTrue(pig.equals(latter));
    }

    @Test
    public void shouldBeEqualWhenNameAndTrainedAreSet() throws Exception {
        pig.setName("cici");
        pig.setTrained(true);
        Pig latter = new Pig();
        latter.setName("cici");
        latter.setTrained(true);
        assertTrue(pig.equals(latter));
    }

    @Test
    public void shouldNotBeEqualWhenTrainedIsDifferent() throws Exception {
        pig.setName("cici");
        pig.setTrained(true);
        Pig latter = new Pig();
        latter.setName("cici");
        latter.setTrained(false);
        assertFalse(pig.equals(latter));
    }

    @Test
    public void shouldNotBeEqualWhenNameIsDifferent() throws Exception {
        pig.setName("cici");
        Pig latter = new Pig();
        latter.setName("burici");
        assertFalse(pig.equals(latter));
    }

    @Test
    public void shouldPrintTypeAndName() throws Exception {
        pig.setName("bubu");
        String expected = "Pig bubu";
        String actual = pig.toString();
        assertEquals(expected, actual);
    }

    @Test
    public void shouldHave10AsRemainingDaysValue() throws Exception {
        assertEquals(10, pig.getSurvivingDays());
    }

    @Test
    public void shouldHave10AsEatingQuantityValue() throws Exception {
        assertEquals(10, pig.getEatingQuantity());
    }

    @Test
    public void shouldBeAlive() throws Exception {
        assertTrue(pig.isAlive());
    }

    @Test
    public void shouldHave0AsDaysWithoutFood() throws Exception {
        assertEquals(0, pig.getDaysWithoutFood());
    }

    @Test
    public void shouldHave1AsDaysWithoutFoodAfterEatingNothing() throws Exception {
        pig.eat(new Food(0));
        assertEquals(1, pig.getDaysWithoutFood());
    }

    @Test
    public void shouldHave0AsDaysWithoutFoodAfterEatingNothing() throws Exception {
        pig.eat(new Food(0));
        pig.eat(new Food(1));
        assertEquals(0, pig.getDaysWithoutFood());
    }
}