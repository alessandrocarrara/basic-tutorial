package it.esteco.somo.farmyard.animals;

import it.esteco.somo.farmyard.Food;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 01, 2015 2:51 PM.
 */
public class CowTest {

    private Cow cow;

    @Before
    public void setUp() throws Exception {
        cow = new Cow();
    }

    @Test
    public void ciciShouldBarkWhenTalking() throws Exception {
        cow.setName("cici");
        String expected = "Mooo Mooo, I am cici";
        String actual = cow.talk();
        assertEquals(expected, actual);
    }

    @Test
    public void noOneShouldBarkWhenTalking() throws Exception {
        String expected = "Mooo Mooo, I am no one";
        String actual = cow.talk();
        assertEquals(expected, actual);
    }

    @Test
    public void shouldBeEqualToSelf() throws Exception {
        assertTrue(cow.equals(cow));
    }

    @Test
    public void shouldHaveSameHashcode() throws Exception {
        Cow latter = new Cow();
        assertEquals(latter.hashCode(), cow.hashCode());
    }

    @Test
    public void shouldNotBeEqualToDifferentObject() throws Exception {
        assertFalse(cow.equals(new Object()));
    }

    @Test
    public void shouldBeEqualWhenNoNameIsSetAndIsNotTrained() throws Exception {
        Cow latter = new Cow();
        assertTrue(cow.equals(latter));
    }

    @Test
    public void shouldNotBeEqualWhenNameIsDifferent() throws Exception {
        cow.setName("cici");
        Cow latter = new Cow();
        latter.setName("burici");
        assertFalse(cow.equals(latter));
    }

    @Test
    public void shouldPrintTypeAndName() throws Exception {
        cow.setName("bubu");
        String expected = "Cow bubu";
        String actual = cow.toString();
        assertEquals(expected, actual);
    }

    @Test
    public void shouldHave10AsRemainingDaysValue() throws Exception {
        assertEquals(10, cow.getSurvivingDays());
    }

    @Test
    public void shouldHave20AsEatingQuantityValue() throws Exception {
        assertEquals(20, cow.getEatingQuantity());
    }

    @Test
    public void shouldBeAlive() throws Exception {
        assertTrue(cow.isAlive());
    }

    @Test
    public void shouldHave0AsDaysWithoutFood() throws Exception {
        assertEquals(0, cow.getDaysWithoutFood());
    }

    @Test
    public void shouldHave1AsDaysWithoutFood() throws Exception {
        cow.eat(new Food(0));
        assertEquals(1, cow.getDaysWithoutFood());
    }

    @Test
    public void shouldHave0AsDaysWithoutFoodAfterEatingSomething() throws Exception {
        cow.eat(new Food(0));
        cow.eat(new Food(1));
        assertEquals(0, cow.getDaysWithoutFood());
    }
}