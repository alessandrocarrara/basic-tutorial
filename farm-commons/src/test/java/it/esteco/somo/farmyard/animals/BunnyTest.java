package it.esteco.somo.farmyard.animals;

import it.esteco.somo.farmyard.Food;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 07, 2015 4:19 PM.
 */
public class BunnyTest {

    private Bunny bunny;

    @Before
    public void setUp() throws Exception {
        bunny = new Bunny();
    }

    @Test
    public void shouldBeEqualToAnotherEmptyBunny() throws Exception {
        Bunny anotherBunny = new Bunny();
        assertEquals(bunny, anotherBunny);
    }

    @Test
    public void shouldHaveSameHashCode() throws Exception {
        Bunny anotherBunny = new Bunny();
        assertEquals(bunny.hashCode(), anotherBunny.hashCode());
    }

    @Test
    public void shouldBeEqualToSelf() throws Exception {
        assertTrue(bunny.equals(bunny));
    }

    @Test
    public void shouldNotBeEqualToDifferentObject() throws Exception {
        assertFalse(bunny.equals(new Object()));
    }
    @Test
    public void shouldSaySqueakWhenTalking() throws Exception {
        bunny.setName("gigi");
        String expected = "Squeak squeak, I am gigi";
        String actual = bunny.talk();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldNotBeEqualWhenNameIsSetOnFormerAndNotInLatter() throws Exception {
        bunny.setName("ciccio");
        Bunny anotherBunny = new Bunny();
        assertNotEquals(bunny, anotherBunny);
    }

    @Test
    public void shouldPrintTypeAndName() throws Exception {
        bunny.setName("bubu");
        String expected = "Bunny bubu";
        String actual = bunny.toString();
        assertEquals(expected, actual);
    }

    @Test
    public void shouldHave2AsRemainingDaysValue() throws Exception {
        assertEquals(2, bunny.getSurvivingDays());
    }

    @Test
    public void shouldHave1AsEatingQuantityValue() throws Exception {
        assertEquals(1, bunny.getEatingQuantity());
    }

    @Test
    public void shouldBeAlive() throws Exception {
        assertTrue(bunny.isAlive());
    }

    @Test
    public void shouldHave0AsDaysWithoutFood() throws Exception {
        assertEquals(0, bunny.getDaysWithoutFood());
    }

    @Test
    public void shouldHave1AsDaysWithoutFoodAfterEatingNothing() throws Exception {
        bunny.eat(new Food(0));
        assertEquals(1, bunny.getDaysWithoutFood());
    }

    @Test
    public void shouldHave0AsDaysWithoutFoodAfterEatingSomething() throws Exception {
        bunny.eat(new Food(0));
        bunny.eat(new Food(1));
        assertEquals(0, bunny.getDaysWithoutFood());
    }
}