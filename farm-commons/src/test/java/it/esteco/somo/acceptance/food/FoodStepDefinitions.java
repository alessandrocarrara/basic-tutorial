package it.esteco.somo.acceptance.food;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import it.esteco.somo.farmyard.Food;
import it.esteco.somo.farmyard.animals.Animal;
import it.esteco.somo.farmyard.animals.AnimalBreed;
import it.esteco.somo.farmyard.animals.AnimalFactory;

import static org.junit.Assert.assertEquals;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Sep 24, 2015 3:32 PM.
 */
public class FoodStepDefinitions {

    private AnimalFactory animalFactory = new AnimalFactory();
    private Animal animal;
    private Food   food;

    @Given("^one animal of breed (bunny|pig|cow)$")
    public void oneAnimalOfBreed(String breed) throws Throwable {
        AnimalBreed animalBreed = AnimalBreed.valueOf(breed.toUpperCase());
        animal = animalFactory.createAnimal(animalBreed, "testAnimal");
    }

    @Given("^a food with quantity (\\d+)$")
    public void aFoodWithQuantity(int foodQuantity) throws Throwable {
        food = new Food(foodQuantity);
    }

    @When("^the animal eats$")
    public void theAnimalEats() throws Throwable {
        animal.eat(food);
    }

    @Then("^the food quantity is (\\d+)$")
    public void theFoodQuantityIs(int remainingQuantity) throws Throwable {
        assertEquals(remainingQuantity, food.getQuantity());
    }

    @When("^the animal eats for (\\d+) times$")
    public void theAnimalEatsForTimes(int times) throws Throwable {
        for (int i = 0; i < times; i++) {
            theAnimalEats();
        }
    }

    @Then("^the animal has (true|false)$")
    public void theAnimalHas(boolean died) throws Throwable {
        assertEquals(died, !animal.isAlive());
    }
}
