package it.esteco.somo.farmyard.animals;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 07, 2015 4:10 PM.
 */
public class Bunny extends Animal {

    @Override
    public String talk() {
        return "Squeak squeak, I am " + getName();
    }

    @Override
    public int eat() {
        return 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Bunny)) {
            return false;
        }

        return super.equals(o);
    }

    @Override
    protected int getSurvivingDays() {
        return 2;
    }

    @Override
    protected int getEatingQuantity() {
        return 1;
    }

}
