package it.esteco.somo.farmyard.exception;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 16, 2015 12:16 PM.
 */
public class UnrecognizedBreedException extends IllegalArgumentException {

    public UnrecognizedBreedException(int type) {
        super("Unrecognized breed type: " + type);
    }
}
