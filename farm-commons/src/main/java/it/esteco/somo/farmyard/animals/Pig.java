package it.esteco.somo.farmyard.animals;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 01, 2015 12:32 PM.
 */
public class Pig extends Animal {

    private boolean trained;

    public boolean isTrained() {
        return trained;
    }

    public void setTrained(boolean trained) {
        this.trained = trained;
    }

    @Override
    public String talk() {
        return "Grunt grunt, I am " + getName();
    }

    @Override
    public int eat() {
        return 10;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pig)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Pig pig = (Pig) o;

        return isTrained() == pig.isTrained();

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (isTrained() ? 1 : 0);
        return result;
    }

    @Override
    protected int getEatingQuantity() {
        return 10;
    }
}
