package it.esteco.somo.farmyard.exception;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 01, 2015 3:15 PM.
 */
public class AnimalException extends Exception {

    public AnimalException(String message) {
        super(message);
    }
}
