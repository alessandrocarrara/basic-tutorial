package it.esteco.somo.farmyard.animals;

import it.esteco.somo.farmyard.Food;

import java.io.Serializable;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 01, 2015 2:46 PM.
 */
public abstract class Animal implements Serializable {

    private String name = "no one";
    private boolean alive = true;
    private int daysWithoutFood = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract String talk();

    public abstract int eat();

    @Override
    public boolean equals(Object o) {

        Animal animal = (Animal) o;

        return !(name != null ? !name.equals(animal.name) : animal.name != null);

    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " " + getName();
    }

    public void eat(Food food) {
        if(!alive) {
            return;
        }
        int eaten = food.remove(getEatingQuantity());
        if(hasEatenSomething(eaten)) {
            daysWithoutFood++;
        } else {
            daysWithoutFood = 0;
        }
        if (daysWithoutFood == getSurvivingDays()) {
            die();
        }
    }

    private boolean hasEatenSomething(int eaten) {
        return eaten == 0;
    }

    protected int getSurvivingDays() {
        return 10;
    }

    protected abstract int getEatingQuantity();

    public boolean isAlive() {
        return alive;
    }

    private void die() {
        this.alive = false;
    }

    public int getDaysWithoutFood() {
        return daysWithoutFood;
    }
}
