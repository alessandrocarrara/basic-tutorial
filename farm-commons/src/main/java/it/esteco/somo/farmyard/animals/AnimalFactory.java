package it.esteco.somo.farmyard.animals;

import it.esteco.somo.farmyard.exception.AnimalException;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 01, 2015 3:10 PM.
 */
public class AnimalFactory {

    private static final String STRAY_ERROR = "Do you want to create a stray?";

    public Cow createCow(String name) throws AnimalException {
        checkName(name);
        Cow cow = new Cow();
        cow.setName(name);
        return cow;
    }

    public Pig createPig(String name, boolean trained) throws AnimalException {
        checkName(name);
        Pig pig = new Pig();
        pig.setName(name);
        pig.setTrained(trained);
        return pig;
    }

    public Bunny createBunny(String name) throws AnimalException {
        checkName(name);
        Bunny bunny = new Bunny();
        bunny.setName(name);
        return bunny;
    }

    private void checkName(String name) throws AnimalException {
        if(name == null) {
            throw new AnimalException(STRAY_ERROR);
        }
    }

    public Animal createAnimal(AnimalBreed animalBreed, String name) throws AnimalException {
        switch (animalBreed) {
            case COW:
                return createCow(name);
            case BUNNY:
                return createBunny(name);
            case PIG:
                return createPig(name, true);
            default:
                throw new IllegalArgumentException();
        }
    }
}
