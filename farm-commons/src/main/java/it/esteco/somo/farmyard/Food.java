package it.esteco.somo.farmyard;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Sep 30, 2015 11:18 AM.
 */
public class Food {

    private int quantity;

    public Food(int quantity) {
        this.quantity = quantity;
    }

    public int remove(int eaten) {
        int oldQuantity = quantity;
        this.quantity = Math.max(0, quantity - eaten);
        return quantity > 0 ? eaten : oldQuantity;
    }

    public int getQuantity() {
        return quantity;
    }
}
