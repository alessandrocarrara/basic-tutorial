package it.esteco.somo.farmyard.animals;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 01, 2015 2:49 PM.
 */
public class Cow extends Animal {

    @Override
    public String talk() {
        return "Mooo Mooo, I am " + getName();
    }

    @Override
    public int eat() {
        return 20;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cow)) {
            return false;
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    protected int getEatingQuantity() {
        return 20;
    }
}
