package it.esteco.somo.farmyard.animals;

import it.esteco.somo.farmyard.exception.UnrecognizedBreedException;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Jul 10, 2015 3:50 PM.
 */
public enum AnimalBreed {
    COW,
    PIG,
    BUNNY;

    public static AnimalBreed fromOrdinal(Integer type) {
        for (AnimalBreed animalBreed : values()) {
            if(animalBreed.ordinal() == type) {
                return animalBreed;
            }
        }
        throw new UnrecognizedBreedException(type);
    }
}
