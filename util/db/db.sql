USE `farmyard`;

CREATE TABLE `animals` (
  `id`   BIGINT(20)                NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128)
         CHARACTER SET 'utf8'
         COLLATE 'utf8_unicode_ci' NOT NULL,
  `type` BIGINT(20)                NOT NULL,
  CONSTRAINT `animalsPK` PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `food` (
  `id`       BIGINT(20) NOT NULL AUTO_INCREMENT,
  `quantity` INT        NOT NULL,
  `type`     BIGINT(20) NOT NULL,
  CONSTRAINT `foodPK` PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;